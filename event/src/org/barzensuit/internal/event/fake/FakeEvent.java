package org.barzensuit.internal.event.fake;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Iterator;

import org.barzensuit.event.IEvent;
import org.barzensuit.event.IEventListener;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;

public class FakeEvent implements IEvent {
	
	/**
	 * @var List<IEventListener> Listeners attached to this event.
	 */
	private ArrayList<IEventListener> listeners;
	
	private Job job;
	
	private boolean isRunning;
	
	private ZonedDateTime now;
	
	private static int FAKE_EVENT_DELAY = 5000;
	
	private static String FAKE_EVENT_ID = "FAKE_EVENT";
	
	public FakeEvent() {
		super();
		listeners = new ArrayList<IEventListener>();
		now = ZonedDateTime.now();
	}
	
	/* (non-Javadoc)
	 * @see org.barzensuit.event.IEvent#addListener(org.barzensuit.event.IEventListener)
	 */
	@Override
	public synchronized void addListener(IEventListener listener) {
		listeners.add(listener);
	}
	
	@Override
	public ZonedDateTime getCreated() {
		return now;
	}

	@Override
	public String getCreatedId() {
		return FAKE_EVENT_ID;
	}

	@Override
	public ZonedDateTime getModified() {
		return now;
	}

	@Override
	public String getModifiedId() {
		return FAKE_EVENT_ID;
	}

	@Override
	public int getStatus() {
		return 0;
	}
	
	/* (non-Javadoc)
	 * @see org.barzensuit.event.IEvent#removeListener(org.barzensuit.event.IEventListener)
	 */
	@Override
	public synchronized void removeListener(IEventListener listener) {
		listeners.remove(listener);
	}
	
	public synchronized void trigger() {
		for (Iterator<IEventListener> i = listeners.iterator(); i.hasNext();)
			((IEventListener) i.next()).wakeup();
	}
	
	public synchronized void shutdown() {
		isRunning = false;
		job.cancel();
		try {
			job.join();
		} catch (InterruptedException e) {
			//
			// Shutting down, safe to ignore
			//
		}
	}
	
	public synchronized void startup() {
		isRunning = true;
		job = new Job(FAKE_EVENT_ID) {
			protected IStatus run(IProgressMonitor monitor) {
				trigger();
				if (isRunning)
					schedule(FAKE_EVENT_DELAY);
				return Status.OK_STATUS;
			}
		};
		job.schedule(FAKE_EVENT_DELAY);
	} 
}
