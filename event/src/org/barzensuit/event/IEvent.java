package org.barzensuit.event;

import java.time.ZonedDateTime;

public interface IEvent {
	/**
	 * Attach a listener to event.
	 */
	public void addListener(IEventListener listener);
	
	/**
	 * @return ZonedDateTime Date and time activity was created.
	 */
	public ZonedDateTime getCreated();
	
	/**
	 * @return ID of user who created activity.
	 * @see IUser.java
	 */
	public String getCreatedId();
	
	/**
	 * @return ZonedDateTime Date and time activity was last modified.
	 */
	public ZonedDateTime getModified();
	
	/**
	 * @return String ID of user who last modified activity.
	 * @see IUser.java
	 */
	public String getModifiedId();
	
	/**
	 * @return int Status of activity.
	 */
	public int getStatus();
	
	/**
	 * Detach a listener from event.
	 */
	public void removeListener(IEventListener listener);

}