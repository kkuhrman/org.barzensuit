package org.barzensuit.event;

public interface IEventListener {
	/**
	 * Event notifies listener standing by of state change by call to wakeup().
	 */
	public void wakeup();
}
