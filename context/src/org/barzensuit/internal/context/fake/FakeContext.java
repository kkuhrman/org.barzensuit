package org.barzensuit.internal.context.fake;

import org.barzensuit.context.IContext;

public class FakeContext implements IContext {

	/**
	 * @var int id Unique id of encapsulated context.
	 */
	private int id;
	
	private static int FAKE_CONTEXT_ID = 1964;
	
	/* (non-Javadoc)
	 * @see org.barzensuit.context.IContext#getId()
	 */
	@Override
	public int getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see org.barzensuit.context.IContext#setId(int)
	 */
	@Override
	public void setId(int id) {
		this.id = id;
	}

	public FakeContext() {
		super();
		this.id = this.FAKE_CONTEXT_ID;
	}

}
