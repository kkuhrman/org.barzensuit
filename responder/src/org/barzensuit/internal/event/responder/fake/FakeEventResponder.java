package org.barzensuit.internal.event.responder.fake;

import org.barzensuit.event.responder.IEventResponder;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;

public class FakeEventResponder implements IEventResponder {
private Job job;
	
	private boolean isRunning;
	
	private static int FAKE_EVENT_RESPONDER_DELAY = 1000;
	
	private static String FAKE_EVENT_RESPONDER_ID = "FAKE_EVENT_RESPONDER";
	
	public FakeEventResponder() {
		super();
	}
	
	/**
	 * Event listener notifies responder of required action by call to dispatch().
	 */
	public synchronized void dispatch() {
		System.out.println("dispatch called");
		isRunning = true;
		job = new Job(FAKE_EVENT_RESPONDER_ID) {
			protected IStatus run(IProgressMonitor monitor) {
				handleDispatch();
				if (isRunning)
					schedule(FAKE_EVENT_RESPONDER_DELAY);
				return Status.OK_STATUS;
			}
		};
		job.schedule(FAKE_EVENT_RESPONDER_DELAY);
	}
	
	/**
	 * The combination of dispatch() and handleDispatch() is intended to 
	 * simulate asynchronous communictions.
	 */
	public synchronized void handleDispatch() {
		//System.out.println("Event responder dispatched: " + FAKE_EVENT_RESPONDER_ID);
		isRunning = false;
		job.cancel();
		System.out.println("dispatch handled");
	}
	
	public synchronized void shutdown() {
		isRunning = false;
		job.cancel();
		try {
			job.join();
		} catch (InterruptedException e) {
			//
			// Shutting down, safe to ignore
			//
		}
	}
	
	public synchronized void startup() {
	}
}
