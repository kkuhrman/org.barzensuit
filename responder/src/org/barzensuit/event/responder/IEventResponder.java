package org.barzensuit.event.responder;

public interface IEventResponder {
	/**
	 * Event listener notifies responder of required action by call to dispatch().
	 */
	public void dispatch();
}
