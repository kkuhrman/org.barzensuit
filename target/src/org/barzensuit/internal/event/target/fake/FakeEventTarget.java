package org.barzensuit.internal.event.target.fake;

import org.barzensuit.event.IEvent;
import org.barzensuit.event.responder.IEventResponder;
import org.barzensuit.event.target.IEventTarget;

public class FakeEventTarget implements IEventTarget {
	
	private IEvent event;
	private IEventResponder responder;

	@Override
	public void wakeup() {
		System.out.println("wakeup called");
		responder.dispatch();
	}

	@Override
	public void setEvent(IEvent event) {
		this.event = event;
	}

	@Override
	public void setResponder(IEventResponder responder) {
		this.responder = responder;
	}
	
	public void shutdown() {
		event.removeListener(this);
	}
	
	public void startup() {
		event.addListener(this);
	}

}
