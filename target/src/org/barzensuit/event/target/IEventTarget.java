/**
 * A concrete instance of a class implementing IEventTarget couples a single 
 * instance of a class implementing IEvent with a single instance of a class
 * implementing IEventResponder. The job of classes implementing IEventTarget
 * is to stand by for a change in state of the encapsulated event and when 
 * notified of such change (by call to wakeup()), examine said change and
 * decide whether to call the encapsulated response (dispatch()).   
 */

package org.barzensuit.event.target;

import org.barzensuit.event.IEvent;
import org.barzensuit.event.IEventListener;
import org.barzensuit.event.responder.IEventResponder;

public interface IEventTarget extends IEventListener {
	/**
	 * Attach event to the target and begin listening.
	 */
	public void setEvent(IEvent event);
	
	/**
	 * Attach responder to the target.
	 */
	public void setResponder(IEventResponder responder);
}
