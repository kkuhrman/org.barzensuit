package org.barzensuit.client;

import org.barzensuit.context.IContext;
import org.barzensuit.event.IEvent;
import org.barzensuit.event.IEventListener;

public class EventMonitor implements IEventListener {
	/**
	 * 
	 */
	private IEvent event;
	
	/**
	 * 
	 */
	private IContext context;
	
	/**
	 * 
	 */
	@Override
	public void wakeup() {
		System.out.println("Event triggered. Context = " + context.getId());
	}
	
	public IEvent getEvent() {
		return event;
	}

	public void setEvent(IEvent event) {
		this.event = event;
	}


	public IContext getContext() {
		return context;
	}

	public void setContext(IContext context) {
		this.context = context;
	}
	
	public void shutdown() {
		event.removeListener(this);
	}
	
	public void startup() {
		event.addListener(this);
	}
}
